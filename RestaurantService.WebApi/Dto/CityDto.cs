﻿namespace Mts.Test.RestaurantWebApi.Dto
{
    /// <summary>
    /// Dto of city
    /// </summary>
    public class CityDto
    {
        /// <summary>
        /// Identifier city
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// Name city
        /// </summary>
        public string Name { get; set; }
    }
}
