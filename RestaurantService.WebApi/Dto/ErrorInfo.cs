﻿namespace Mts.Test.RestaurantWebApi.Dto
{

    /// <summary>
    /// Information about error
    /// </summary>
    public class ErrorInfo
    {
        /// <summary>
        /// Code error
        /// </summary>
        public int Code { get;  }

        /// <summary>
        /// Message error
        /// </summary>
        public string Message { get; }


        public ErrorInfo(int code, string message)
        {
            Code = code;
            Message = message;
        }
    }
}
