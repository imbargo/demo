﻿using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Mts.Test.RestaurantWebApi.Dto
{
    //Page 
    internal sealed class Page<T>
    {
        [UsedImplicitly]
        public Page(IEnumerable<T> items, int numberPage, int totalPages)
        {
            NumberPage = numberPage;
            Items = items.ToList();
            TotalPages = totalPages;
        }

        public Page(IEnumerable<T> items, int skip, int limit, int totalCount)
        {
            if (items == null)
                throw new ArgumentNullException(nameof(items));
            NumberPage = 0;
            TotalPages = 0;

            if (limit > 0)
            {
                NumberPage = skip / limit + 1;
                TotalPages = (int)Math.Ceiling((double)totalCount / limit);
            }
            Items = items.ToList();
        }

        public int NumberPage { get; }

        public int TotalPages { get; }

        public IEnumerable<T> Items { get; }

    }
}
