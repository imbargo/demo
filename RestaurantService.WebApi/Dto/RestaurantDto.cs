﻿namespace Mts.Test.RestaurantWebApi.Dto
{
    /// <summary>
    /// Dto restaurant
    /// </summary>
    public class RestaurantDto
    {
        /// <summary>
        /// Identifier
        /// </summary>
        public int? Id { get; set; }

        /// <summary>
        /// Name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// City of restaurant
        /// </summary>
        public CityDto City { get; set; }
    }
}
