﻿using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;
using Mts.Test.RestaurantCore;
using Mts.Test.RestaurantWebApi.Dto;
using Mts.Test.RestaurantWebApi.Utils.Extensions;
using System.Threading.Tasks;

namespace Mts.Test.RestaurantWebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CitiesController
    {
        private readonly ICityService _cityService;
        private readonly IRestaurantService _restaurantService;

        [UsedImplicitly]
        public CitiesController(ICityService cityService, IRestaurantService restaurantService)
        {
            _cityService = cityService;
            _restaurantService = restaurantService;
        }

        /// <summary>
        /// Return page cities. 
        /// </summary>
        /// <param name="skip">Number to skip</param>
        /// <param name="limit">Max number to take</param>
        /// <returns>Page list</returns>
        [HttpGet]
        [UsedImplicitly]
        public IActionResult Get([FromQuery] int skip, int limit)
        {
            var pagination = _cityService.GetAll(skip, limit);
            var cities = pagination.Items.ToDto();
            var cityPage =
                new Page<CityDto>(cities, skip, limit, pagination.TotalCount);
            return new OkObjectResult(cityPage);
        }

        /// <summary>
        /// Return city by id
        /// </summary>
        /// <param name="id">Identifier city</param>
        /// <returns>City dto</returns>
        [HttpGet]
        [Route("{id}")]
        public IActionResult Get(int id)
        {
            var city = _cityService.Get(id);
            return new OkObjectResult(city);
        }

        //[HttpGet]
        //[Route("{id}/restaurants")]
        //public IActionResult GetRestaurants(int id, [FromQuery] int skip, int limit)
        //{
        //    var pagination = _restaurantService.FindByCity(id, skip, limit);
        //    var restaurants = pagination.Items.ToDto();
        //    var restaurantsPage =
        //        new Page<RestaurantDto>(restaurants, skip, limit, pagination.TotalCount);
        //    return new OkObjectResult(restaurantsPage);
        //}

        /// <summary>
        /// Return all restaurants for city
        /// </summary>
        /// <param name="id">Identifier city</param>
        /// <param name="skip">Number to skip</param>
        /// <param name="limit">Max number to take</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{id}/restaurants")]
        public async Task<IActionResult> GetRestaurants(int id, [FromQuery] int skip, int limit)
        {
            var pagination = await _restaurantService.FindByCityAsync(id, skip, limit);
            var restaurants = pagination.Items.ToDto();
            var restaurantsPage =
                new Page<RestaurantDto>(restaurants, skip, limit, pagination.TotalCount);
            return new OkObjectResult(restaurantsPage);
        }

        /// <summary>
        /// Add new city
        /// </summary>
        /// <param name="city">City for add</param>
        /// <returns>Return added city</returns>
        [HttpPost]
        [UsedImplicitly]
        public IActionResult Add([FromBody] CityDto city)
        {
            var newCity = _cityService.Add(city.ToDomain());
            return new OkObjectResult(newCity);
        }


    }
}
