﻿using JetBrains.Annotations;
using Microsoft.AspNetCore.Mvc;
using Mts.Test.RestaurantCore;
using Mts.Test.RestaurantWebApi.Dto;
using Mts.Test.RestaurantWebApi.Utils.Extensions;

namespace Mts.Test.RestaurantWebApi.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class RestaurantsController
    {
        private readonly IRestaurantService _restaurantService;

        [UsedImplicitly]
        public RestaurantsController(IRestaurantService restaurantService)
        {
            _restaurantService = restaurantService;
        }

        //// GET api/v1/restaurants{skip}
        //[HttpGet]
        //public async Task<IActionResult> Get([FromQuery] int skip, int limit)
        //{
        //    var restaurants = _restaurantService.GetAll(skip, limit);
        //    var response = new Response<Page<IEnumerable<IRestaurant>>>();
        //    response.Data = new Page<IEnumerable<IRestaurant>>();
        //    return response;
        //}

        /// <summary>
        /// Return page restaurants. 
        /// </summary>
        /// <param name="skip">Number to skip</param>
        /// <param name="limit">Number to take max</param>
        /// <returns>Page list</returns>
        [HttpGet]
        [UsedImplicitly]
        public IActionResult Get([FromQuery] int skip, int limit)
        {
            var pagination = _restaurantService.GetAll(skip, limit);
            var restaurants = pagination.Items.ToDto();
            var restaurantsPage = 
                new Page<RestaurantDto>(restaurants, skip, limit, pagination.TotalCount);
            return new OkObjectResult(restaurantsPage);
        }

        /// <summary>
        /// Return restaurant by id
        /// </summary>
        /// <param name="id">Identifier restaurant</param>
        /// <returns>Return restaurant by id</returns>
        [HttpGet]
        [Route("{id}")]
        public IActionResult Get(int id)
        {
            var restaurant = _restaurantService.Get(id)
                .ToDto();
            return new OkObjectResult(restaurant);
        }

        /// <summary>
        /// Add new restaurant
        /// </summary>
        /// <param name="restaurant">Restaurant for add</param>
        /// <returns>Return added restaurant</returns>
        [HttpPost]
        [UsedImplicitly]
        public IActionResult Add([FromBody] RestaurantDto restaurant)
        {
            var newRestaurant = _restaurantService.Add(restaurant.ToDomain());
            return new OkObjectResult(newRestaurant.ToDto());
        }
    }
}
