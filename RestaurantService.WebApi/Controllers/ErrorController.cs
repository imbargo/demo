﻿using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Mts.Test.RestaurantWebApi.Dto;

namespace Mts.Test.RestaurantWebApi.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [ApiController]
    public class ErrorController : ControllerBase
    {
        [HttpGet]
        [Route("/error")]
        public ActionResult<ErrorInfo> Error()
        {
            var context = HttpContext.Features.Get<IExceptionHandlerFeature>();
            const int code = 500;
            var error = new ErrorInfo(code, context?.Error.Message);
            Response.StatusCode = code; 
            return error; 
        }


    }
}
