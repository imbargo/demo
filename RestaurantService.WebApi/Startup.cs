﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Mts.Test.RestaurantCore;
using Mts.Test.RestaurantCore.Repositories;
using System;
using System.Reflection;
using System.IO;
using Microsoft.OpenApi.Models;

namespace Mts.Test.RestaurantWebApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            // ReSharper disable once UnusedVariable
            var connectionString = Configuration["Data:DefaultConnection:ConnectionString"];
            //services.AddSingleton<IRestaurantRepository, RestaurantRepositoryMock>();
            //services.AddSingleton<ICityRepository, CityRepositoryMock>();
            services.AddSingleton<ICityRepository>(rep => new CityRepositoryMsSql(connectionString));
            services.AddSingleton<IRestaurantRepository>(rep => new RestaurantRepositoryMsSql(connectionString));
            services.AddSingleton<IRestaurantService, RestaurantService>();
            services.AddSingleton<ICityService, CityService>();

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Restaurant demo API",
                    Description = "A demo test ASP.NET Core Web API"
                });
                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });
        }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            env.EnvironmentName = "prod";
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //else
            //{
            //    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            //    app.UseHsts();
            //}
            else
            {
                app.UseExceptionHandler("/error");
            }

            app.UseHttpsRedirection();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                c.RoutePrefix = "api/v1/help";
            });

            app.UseMvc();
        }
    }
}
