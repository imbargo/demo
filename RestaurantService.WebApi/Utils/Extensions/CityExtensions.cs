﻿using System.Collections.Generic;
using System.Linq;
using Mts.Test.RestaurantCore.Models;
using Mts.Test.RestaurantWebApi.Dto;

namespace Mts.Test.RestaurantWebApi.Utils.Extensions
{
    //NOTE: при увеличении количества классов лучше использовать AutoMapper

    internal static class CityExtensions
    {
        public static CityDto ToDto(this ICity source)
        {
            return new CityDto
            {
                Id = source.Id,
                Name = source.Name
            };
        }

        public static IEnumerable<CityDto> ToDto(this IEnumerable<ICity> source)
        {
            return source.Select(r => r.ToDto());
        }
    }

    internal static class CityDtoExtensions
    {
        public static ICity ToDomain(this CityDto source)
        {
            return new City(source.Id, source.Name);
        }
    }
}