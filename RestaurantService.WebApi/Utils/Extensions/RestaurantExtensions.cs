﻿using System.Collections.Generic;
using System.Linq;
using Mts.Test.RestaurantCore.Models;
using Mts.Test.RestaurantWebApi.Dto;

namespace Mts.Test.RestaurantWebApi.Utils.Extensions
{
    //NOTE: при увеличении количества классов лучше использовать AutoMapper

    internal static class RestaurantExtensions
    {
        public static RestaurantDto ToDto(this IRestaurant source)
        {
            return new RestaurantDto
            {
                Id = source.Id,
                Name = source.Name,
                City = source.City.ToDto()
            };
        }

        public static IEnumerable<RestaurantDto> ToDto(this IEnumerable<IRestaurant> source)
        {
            return source.Select(r=>r.ToDto());
        }

    }

    internal static class RestaurantDtoExtensions
    {
        public static IRestaurant ToDomain(this RestaurantDto source)
        {
            return new Restaurant(source.Id, source.Name, source.City.ToDomain());

        }
    }
}
