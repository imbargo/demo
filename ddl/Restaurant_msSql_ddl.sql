/* --����������������� ��� ���������� �������
ALTER DATABASE [MtsTest] SET  SINGLE_USER WITH ROLLBACK IMMEDIATE
DROP DATABASE [MtsTest]
DROP LOGIN [RestaurantService]
*/
CREATE DATABASE [MtsTest]

GO
--
--�������� ������������
CREATE LOGIN [RestaurantService] WITH PASSWORD=N'RestaurantService', DEFAULT_DATABASE=[MtsTest], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF
GO
DECLARE @sql AS NVARCHAR(MAX);
SET @sql =
'USE [MtsTest] 
CREATE USER [RestaurantService] FOR LOGIN [RestaurantService]
ALTER USER [RestaurantService] WITH DEFAULT_SCHEMA=[dbo]
EXEC sp_addrolemember N''db_owner'', N''RestaurantService''
'
EXEC(@sql)
GO

CREATE TABLE [MtsTest].[dbo].[City](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NULL,
	CONSTRAINT [PK_City] PRIMARY KEY CLUSTERED ([Id] ASC)
)

CREATE TABLE [MtsTest].[dbo].[Restaurant](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NULL,
	[CityId] [int] NULL
	CONSTRAINT [PK_Restaurant] PRIMARY KEY CLUSTERED ([Id] ASC)
)

ALTER TABLE [MtsTest].[dbo].[Restaurant]  WITH CHECK ADD  CONSTRAINT [FK_Restaurant_City] FOREIGN KEY([CityId])
REFERENCES [MtsTest].[dbo].[City] ([Id])
ALTER TABLE [MtsTest].[dbo].[Restaurant] CHECK CONSTRAINT [FK_Restaurant_City]
-----------------------------------------------------------------------------
--������ ��� ���� ������
INSERT INTO [MtsTest].[dbo].[City] ([Name])
VALUES ('������')

INSERT INTO [MtsTest].[dbo].[City] ([Name])
VALUES ('�����-���������')

INSERT INTO [MtsTest].[dbo].[Restaurant]([Name], [CityId])
VALUES ('�����' , 1)

INSERT INTO [MtsTest].[dbo].[Restaurant]( [Name], [CityId])
VALUES ('�������' , 1)

INSERT INTO [MtsTest].[dbo].[Restaurant]( [Name], [CityId])
VALUES ('���' , 1)

INSERT INTO [MtsTest].[dbo].[Restaurant]([Name], [CityId])
VALUES ('�������', 2)

INSERT INTO [MtsTest].[dbo].[Restaurant]([Name], [CityId])
VALUES ('����-����', 2)

INSERT INTO [MtsTest].[dbo].[Restaurant]([Name], [CityId])
VALUES ('�����', 2)
