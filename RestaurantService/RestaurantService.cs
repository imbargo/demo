﻿using JetBrains.Annotations;
using Mts.Test.RestaurantCore.Models;
using Mts.Test.RestaurantCore.Repositories;
using System;
using System.Threading.Tasks;
using Mts.Test.RestaurantCore.Utils;

namespace Mts.Test.RestaurantCore
{
    /// <inheritdoc />
    public class RestaurantService : IRestaurantService
    {
        private readonly IRestaurantRepository _restaurantRepository;
        private readonly ICityRepository _cityRepository;

        [UsedImplicitly]
        public RestaurantService(IRestaurantRepository restaurantRepository, ICityRepository cityRepository)
        {
            _restaurantRepository = restaurantRepository;
            _cityRepository = cityRepository;
        }
        
        /// <inheritdoc />
        public IRestaurant Get(int id)
        {
            var restaurant = _restaurantRepository.Get(id);
            if(restaurant == null)
                throw new Exception($"Restaurant with {nameof(id)}={id} not exists");
            return restaurant;
        }

        /// <inheritdoc />
        public Pagination<IRestaurant> GetAll(int skip, int limit)
        {
            Check.AboveOrEqualZero(skip, nameof(skip));
            Check.AboveZero(limit, nameof(limit));

            return _restaurantRepository.GetAll(skip, limit);
        }

        /// <inheritdoc />
        public Pagination<IRestaurant> FindByCity(int cityId, int skip, int limit)
        {
            Check.AboveOrEqualZero(skip, nameof(skip));
            Check.AboveZero(limit, nameof(limit));
            var city = GetCity(cityId);
            // ReSharper disable once ConvertIfStatementToReturnStatement
            if (city == null)
                return Pagination<IRestaurant>.Empty;

            return _restaurantRepository.FindByCity(city, skip, limit);
        }

        /// <inheritdoc />
        public Task<Pagination<IRestaurant>> FindByCityAsync(int cityId, int skip, int limit)
        {
            Check.AboveOrEqualZero(skip, nameof(skip));
            Check.AboveZero(limit, nameof(limit));
            var city = GetCity(cityId);
            // ReSharper disable once ConvertIfStatementToReturnStatement
            if (city == null)
                return Task.Run(() => Pagination<IRestaurant>.Empty);

            return Task.Run(()=>_restaurantRepository.FindByCity(city, skip, limit));
        }

        /// <inheritdoc />
        public IRestaurant Add(IRestaurant restaurant)
        {
            CheckRestaurant(restaurant);

            // ReSharper disable once PossibleInvalidOperationException
            CheckExists(restaurant.City.Id.Value);

            CheckNotExists(restaurant);

            var newRestaurant = _restaurantRepository.Add(restaurant);
            return newRestaurant;
        }

        //Проверяет данные по ресторану, если данные некорректны вернется Exception
        private void CheckRestaurant(IRestaurant restaurant)
        {
            if (restaurant == null)
                throw new NullReferenceException($"{nameof(restaurant)}");

            if (string.IsNullOrWhiteSpace(restaurant.Name))
                throw new ArgumentException("Name is null or whitespace");

            if (restaurant.City == null)
                throw new NullReferenceException("City is null");

            if (restaurant.City.Id == null)
                throw new NullReferenceException("City id is null");
        }

        //проверяет что город существует, если город не существует, возникнет Exception
        private void CheckExists(int idCity)
        {
            var city = _cityRepository.Get(idCity);
            if (city is null)
                throw new Exception($"City with id={idCity} not exists");
        }

        //проверяет что ресторан не существует, если ресторан существует, возникнет Exception
        private void CheckNotExists(IRestaurant restaurant)
        {
            if (_restaurantRepository.Exists(restaurant))
                throw new Exception("Restaurant already exists");
        }

        //Получает город по id
        private ICity GetCity(int idCity)
        {
            var city = _cityRepository.Get(idCity);
            return city;
        }
    }
}
