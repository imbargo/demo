﻿using JetBrains.Annotations;

namespace Mts.Test.RestaurantCore.Models
{
    public sealed class Restaurant : IRestaurant
    {
        [UsedImplicitly]
        public Restaurant(string name, ICity city) :
            this(null, name, city)
        {
        }

        public Restaurant(int? id, string name, ICity city)
        {
            Id = id;
            Name = name;
            City = city;
        }

        /// <inheritdoc />
        public int? Id { get; }

        /// <inheritdoc />
        public string Name { get; }

        /// <inheritdoc />
        public ICity City { get; }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (ReferenceEquals(this, obj))
                return true;
            var instObj = obj as Restaurant;
            return Equals(instObj);
        }

        public bool Equals(Restaurant obj)
        {
            //NOTE: id не учитывается
            if (City != null)
                return Name ==  obj.Name && City.Equals(obj.City);
            if (obj.City != null)
                return Name == obj.Name && obj.City.Equals(City);

            return Name.Equals(obj.Name);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            unchecked
            {
                const int hashingBase = (int)2166136261;
                const int hashingMultiplier = 16777619;
                var hash = hashingBase;
                hash = (hash * hashingMultiplier) ^ (!ReferenceEquals(null, Name) ? Name.GetHashCode() : 0);
                hash = (hash * hashingMultiplier) ^ (!ReferenceEquals(null, City) ? City.GetHashCode() : 0);
                return hash;
            }
        }
    }
}
