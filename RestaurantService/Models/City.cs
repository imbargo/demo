﻿namespace Mts.Test.RestaurantCore.Models
{

    public sealed class City : ICity
    {

        public City(int? id, string name)
        {
            Id = id;
            Name = name;
        }

        /// <inheritdoc />
        public int? Id { get; }

        /// <inheritdoc />
        public string Name { get; }

        /// <inheritdoc />
        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (ReferenceEquals(this, obj))
                return true;
            var cityObj = obj as City;
            return Equals(cityObj);
        }

        public bool Equals(City obj)
        {
            //NOTE: id не учитывается
            return Name.Equals(obj.Name);
        }

        /// <inheritdoc />
        public override int GetHashCode()
        {
            unchecked
            {
                const int hashingBase = (int)2166136261;
                const int hashingMultiplier = 16777619;
                var hash = hashingBase;
                hash = (hash * hashingMultiplier) ^ (!ReferenceEquals(null, Name) ? Name.GetHashCode() : 0);
                return hash;
            }
        }
    }
}