﻿namespace Mts.Test.RestaurantCore.Models
{
    /// <summary>
    /// Ресторан
    /// </summary>
    public interface IRestaurant
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        int? Id { get; } 

        /// <summary>
        /// Имя
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Город
        /// </summary>
        ICity City { get; }
    }
}
