﻿namespace Mts.Test.RestaurantCore.Models
{
    /// <summary>
    /// Город
    /// </summary>
    public interface ICity
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        int? Id { get; }

        /// <summary>
        /// Имя
        /// </summary>
        string Name { get; }
    }
}
