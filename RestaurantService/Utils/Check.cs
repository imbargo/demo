﻿using System;

namespace Mts.Test.RestaurantCore.Utils
{

    /// <summary>
    /// Вспомогательный класс для проверки утверждений
    /// </summary>
    internal static class Check
    {
        /// <summary>
        /// Значение больше или равно нулю
        /// </summary>
        public static void AboveOrEqualZero(int value, string paramName)
        {
            if (value < 0)
                throw new ArgumentOutOfRangeException(paramName);
        }

        /// <summary>
        /// Значение больше нуля
        /// </summary>
        public static void AboveZero(int value, string paramName)
        {
            if (value <= 0)
                throw new ArgumentOutOfRangeException(nameof(paramName));
        }


    }
}
