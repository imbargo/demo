﻿using System.Threading.Tasks;
using Mts.Test.RestaurantCore.Models;
using Mts.Test.RestaurantCore.Repositories;

namespace Mts.Test.RestaurantCore
{
    /// <summary>
    /// Сервис по работе с ресторанами
    /// </summary>
    public interface IRestaurantService
    {
        /// <summary>
        /// //Получает ресторан по <param name="id"></param>
        /// </summary>
        /// <param name="id">Идентификатор ресторана</param>
        /// <returns>Возвращяет ресторан по id</returns>
        IRestaurant Get(int id);

        /// <summary>
        /// Получает страницу с ресторанами
        /// </summary>
        /// <param name="skip">Число элементов, которое нужно пропустить</param>
        /// <param name="limit">Число элементов, которое нужно взять</param>
        /// <returns>Возвращяет страницу с ресторанами</returns>
        Pagination<IRestaurant> GetAll(int skip, int limit);

        /// <summary>
        /// Добавляет новый ресторан
        /// </summary>
        /// <param name="restaurant">Новый ресторан</param>
        /// <returns>Возвращяет добавленный ресторан</returns>
        IRestaurant Add(IRestaurant restaurant);

        /// <summary>
        /// Возвращает страницу с ресторанами по id города
        /// </summary>
        /// <param name="cityId">Id города</param>
        /// <param name="skip">Число элементов, которое нужно пропустить</param>
        /// <param name="limit">Число элементов, которое нужно взять</param>
        /// <returns>Возвращяет страницу с ресторанами</returns>
        Pagination<IRestaurant> FindByCity(int cityId, int skip, int limit);

        /// <summary>
        /// <see cref="FindByCity"/>
        /// </summary>
        Task<Pagination<IRestaurant>> FindByCityAsync(int cityId, int skip, int limit);
    }
}
