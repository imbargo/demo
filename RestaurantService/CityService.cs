﻿using System;
using JetBrains.Annotations;
using Mts.Test.RestaurantCore.Models;
using Mts.Test.RestaurantCore.Repositories;
using Mts.Test.RestaurantCore.Utils;

namespace Mts.Test.RestaurantCore
{
    /// <inheritdoc />
    public class CityService : ICityService
    {
        private readonly ICityRepository _cityRepository;

        [UsedImplicitly]
        public CityService(ICityRepository cityRepository)
        {
            _cityRepository = cityRepository;
        }


        /// <inheritdoc />
        public ICity Get(int id)
        {
            var city = _cityRepository.Get(id);
            if (city == null)
                throw new Exception($"City with {nameof(id)}={id} not exists");
            return city;
        }


        /// <inheritdoc />
        public Pagination<ICity> GetAll(int skip, int limit)
        {
            Check.AboveOrEqualZero(skip, nameof(skip));
            Check.AboveZero(limit, nameof(limit));
            return _cityRepository.GetAll(skip, limit);
        }

        /// <inheritdoc />
        public ICity Add(ICity city)
        {
            CheckCity(city);

            CheckNotExistsCity(city);

            var newCity = _cityRepository.Add(city);
            return newCity;
        }

        //проверяет данные по городу, если данные некорректны вернется Exception
        private static void CheckCity(ICity city)
        {
            if (city == null)
                throw new NullReferenceException($"{nameof(city)}");

            if (string.IsNullOrWhiteSpace(city.Name))
                throw new ArgumentException("Name is null or whitespace");
        }

        //проверяет что город не существует, если город существует, возникнет Exception
        private void CheckNotExistsCity(ICity city)
        {
            if (_cityRepository.Exists(city))
                throw new Exception("City already exists");
        }

    }
}
