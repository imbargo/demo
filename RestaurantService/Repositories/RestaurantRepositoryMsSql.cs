﻿using Dapper;
using JetBrains.Annotations;
using Mts.Test.RestaurantCore.Models;
using System.Data.SqlClient;
using System.Linq;

namespace Mts.Test.RestaurantCore.Repositories
{
    /// <summary>
    /// Репозиторий ресторанов MS SQL.
    /// </summary>
    public class RestaurantRepositoryMsSql : IRestaurantRepository
    {
        private readonly string _connectionString;

        [UsedImplicitly]
        public RestaurantRepositoryMsSql(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <inheritdoc />
        public IRestaurant Add(IRestaurant restaurant)
        {
            const string sqlScript = @"
                    INSERT INTO [Restaurant]([Name], [CityId]) VALUES(@name, @cityId)
                    SELECT R.[Id], R.Name, [CityId] = C.Id, [CityName] = C.Name 
                    FROM [Restaurant] AS R
                        JOIN [City] AS C ON R.CityId = C.Id
                    WHERE R.Id = SCOPE_IDENTITY()";

            using (var connection = new SqlConnection(_connectionString))
            {
                var row = connection.Query(sqlScript,
                    new
                    {
                        name = restaurant.Name,
                        cityId = restaurant.City.Id
                    }).Single();
                return new Restaurant(row.Id, row.Name,
                        new City(row.CityId, row.CityName));
            }

        }

        public Pagination<IRestaurant> GetAll(int skip, int limit)
        {
            const string sqlScript = @"
                    SELECT *
                    FROM (
	                    SELECT R.[Id], R.Name, [CityId] = C.Id, [CityName] = C.Name, RowNum = ROW_NUMBER() OVER(ORDER BY R.Id)  
	                    FROM [Restaurant] AS R
		                    JOIN [City] AS C ON R.CityId = C.Id
	                    ) AS [Restaurant1]
                    WHERE [Restaurant1].RowNum > @skip AND [Restaurant1].RowNum <= @skip + @limit

                    SELECT [TotalCount] = COUNT(Id)
                    FROM [Restaurant]";

            using (var connection = new SqlConnection(_connectionString))
            {
                var queryMultiple = connection.QueryMultiple(sqlScript,
                    new
                    {
                        skip,
                        limit
                    });
                var restaurants = queryMultiple.Read()
                    .ToList()
                    .Select(row => 
                        new Restaurant(row.Id, row.Name, new City(row.CityId, row.CityName)));
                var totalCount = queryMultiple.Read().Single().TotalCount;
                return new Pagination<IRestaurant>(restaurants, totalCount);
            }
        }

        /// <inheritdoc />
        public IRestaurant Get(int id)
        {
            const string sqlScript = @"
	                    SELECT R.[Id], R.Name, [CityId] = C.Id, [CityName] = C.Name, RowNum = ROW_NUMBER() OVER(ORDER BY R.Id)  
	                    FROM [Restaurant] AS R
		                    JOIN [City] AS C ON R.CityId = C.Id
                        WHERE R.Id = @id";

            using (var connection = new SqlConnection(_connectionString))
            {
                var query = connection.Query(sqlScript,
                    new
                    {
                        id
                    });
                var row = query.Single();
                return new Restaurant(row.Id, row.Name,
                    new City(row.CityId, row.CityName));
            }
        }

        public Pagination<IRestaurant> FindByCity(ICity city, int skip, int limit)
        {
            const string sqlScript = @"
                    SELECT *
                    FROM (
	                    SELECT R.[Id], R.Name, [CityId] = C.Id, [CityName] = C.Name, RowNum = ROW_NUMBER() OVER(ORDER BY R.Id)  
	                    FROM [Restaurant] AS R
		                    JOIN [City] AS C ON R.CityId = C.Id
                        WHERE C.Id = @id
	                    ) AS [Restaurant1]
                    WHERE [Restaurant1].RowNum > @skip AND [Restaurant1].RowNum <= @skip + @limit

                    SELECT [TotalCount] = COUNT(Id)
                    FROM [Restaurant] AS R
                    WHERE R.CityId = @id";

            using (var connection = new SqlConnection(_connectionString))
            {
                var queryMultiple = connection.QueryMultiple(sqlScript,
                    new
                    {
                        id = city.Id,
                        skip,
                        limit
                    });
                var restaurants = queryMultiple.Read()
                    .Select(row =>
                        new Restaurant(row.Id, row.Name, new City(row.CityId, row.CityName)))
                    .ToList();
                var totalCount = queryMultiple.Read().Single().TotalCount;
                return new Pagination<IRestaurant>(restaurants, totalCount);
            }
        }

        /// <inheritdoc />
        public bool Exists(IRestaurant restaurant)
        {
            const string sqlScript = @"
	                    SELECT [TotalCount] = COUNT(Id)
                        FROM [Restaurant] AS R
                        WHERE LOWER(R.Name) = @name AND R.CityId = @cityId";

            using (var connection = new SqlConnection(_connectionString))
            {
                var query = connection.Query(sqlScript,
                    new
                    {
                        name = restaurant.Name.ToLower(),
                        cityId = restaurant.City.Id
                    });
                return query.Single().TotalCount > 0;
            }
        }
    }
}
