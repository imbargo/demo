﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Mts.Test.RestaurantCore.Models;

namespace Mts.Test.RestaurantCore.Repositories
{
    /// <inheritdoc />
    /// <summary>
    /// Репозиторий городов в памяти
    /// </summary>
    public class CityRepositoryMock : ICityRepository
    {
        private readonly Dictionary<int, ICity> _cities;

        [UsedImplicitly]
        public CityRepositoryMock()
        {
            _cities = new Dictionary<int, ICity>
            {
                { 1, new City(1, "Санкт-Петербург")},
                { 2, new City(2, "Москва")}
            };

        }

        public ICity Add(ICity city)
        {
            var nextId = _cities.Keys.Max() + 1;
            var newCity = new City(nextId, city.Name);
            _cities.Add(nextId, newCity);
            return newCity;
        }

        public Pagination<ICity> GetAll(int skip, int limit)
        {
            var cities = _cities.Values.ToList()
                .Skip(skip).Take(limit).ToList();
            var totalCount = _cities.Values.ToList().Count;
            return new Pagination<ICity>(cities, totalCount);
        }

        public ICity Get(int id)
        {
            if (_cities.TryGetValue(id, out var city))
            {
                return city;
            }
            throw new Exception($"City with {nameof(id)}={id} not exists.");
        }

        /// <inheritdoc />
        public bool Exists(ICity city)
        {
            return _cities
                .Any(r => r.Value.Name.Equals(city.Name));
        }
    }
}