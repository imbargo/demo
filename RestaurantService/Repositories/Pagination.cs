﻿using System.Collections.Generic;
using System.Linq;

namespace Mts.Test.RestaurantCore.Repositories
{

    public class Pagination<T>
    {
        public Pagination(IEnumerable<T> items, int totalCount)
        {
            Items = items.ToList();
            TotalCount = totalCount;
        }

        public int TotalCount { get; }

        public IEnumerable<T> Items { get; }

        public static Pagination<T> Empty => new Pagination<T>(new List<T>(), 0);
    }
}
