﻿using Dapper;
using JetBrains.Annotations;
using Mts.Test.RestaurantCore.Models;
using System.Data.SqlClient;
using System.Linq;

namespace Mts.Test.RestaurantCore.Repositories
{
    /// <summary>
    /// Репозиторий городов MS SQL.
    /// </summary>
    public class CityRepositoryMsSql : ICityRepository
    {
        private readonly string _connectionString;

        [UsedImplicitly]
        public CityRepositoryMsSql(string connectionString)
        {
            _connectionString = connectionString;
        }

        public ICity Add(ICity city)
        {
            const string sqlScript = @"
                    INSERT INTO [City]([Name]) VALUES(@name)
                    SELECT C.[Id], C.[Name]
                    FROM [City] AS C
                    WHERE C.Id = SCOPE_IDENTITY()";

            using (var connection = new SqlConnection(_connectionString))
            {
                var row = connection.Query(sqlScript,
                    new
                    {
                        name = city.Name
                    }).Single();
                return new City(row.Id, row.Name);
            }
        }

        public Pagination<ICity> GetAll(int skip, int limit)
        {
            const string sqlScript = @"
                    SELECT *
                    FROM (
                        SELECT C.[Id], C.[Name], RowNum = ROW_NUMBER() OVER(ORDER BY C.Id) 
                        FROM [City] AS C
	                    ) AS [C1]
                    WHERE [C1].RowNum > @skip AND [C1].RowNum <= @skip + @limit

                    SELECT [TotalCount] = COUNT(Id)
                    FROM [City]";

            using (var connection = new SqlConnection(_connectionString))
            {
                var queryMultiple = connection.QueryMultiple(sqlScript,
                    new
                    {
                        skip,
                        limit
                    });
                var cities = queryMultiple.Read()
                    .Select(row => new City(row.Id, row.Name))
                    .ToList();
                var totalCount = queryMultiple.Read().Single().TotalCount;
                return new Pagination<ICity>(cities, totalCount);
            }
        }

        public ICity Get(int id)
        {
            const string sqlScript = @"
	                    SELECT C.[Id], C.[Name]
                        FROM [City] AS C
                        WHERE C.Id = @id";

            using (var connection = new SqlConnection(_connectionString))
            {
                var query = connection.Query(sqlScript,
                    new
                    {
                        id
                    });
                var row = query.Single();
                return new City(row.Id, row.Name);
            }
        }

        /// <inheritdoc />
        public bool Exists(ICity city)
        {
            const string sqlScript = @"
	                    SELECT [TotalCount] = COUNT(Id)
                        FROM [City] AS C
                        WHERE LOWER(C.Name) = @name";

            using (var connection = new SqlConnection(_connectionString))
            {
                var query = connection.Query(sqlScript,
                    new
                    {
                        name = city.Name.ToLower()
                    });
                return query.Single().TotalCount > 0;
            }
        }
    }
}