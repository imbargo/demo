﻿using Mts.Test.RestaurantCore.Models;

namespace Mts.Test.RestaurantCore.Repositories
{
    /// <summary>
    /// Репозиторий для ресторанов
    /// </summary>
    public interface IRestaurantRepository
    {
        IRestaurant Add(IRestaurant restaurant);

        IRestaurant Get(int id);

        Pagination<IRestaurant> GetAll(int skip, int limit);

        Pagination<IRestaurant> FindByCity(ICity city, int skip, int limit);

        bool Exists (IRestaurant restaurant);
    }
}
