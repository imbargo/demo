﻿using Mts.Test.RestaurantCore.Models;

namespace Mts.Test.RestaurantCore.Repositories
{
    /// <summary>
    /// Репозиторий для городов
    /// </summary>
    public interface ICityRepository
    {
        ICity Add(ICity restaurant);

        Pagination<ICity> GetAll(int skip, int limit);

        ICity Get(int id);

        bool Exists(ICity city);
    }
}
