﻿using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;
using Mts.Test.RestaurantCore.Models;

namespace Mts.Test.RestaurantCore.Repositories
{
    /// <summary>
    /// Репозиторий ресторанов в памяти.
    /// </summary>
    public class RestaurantRepositoryMock : IRestaurantRepository
    {
        private readonly Dictionary<int, IRestaurant> _restaurants;

        [UsedImplicitly]
        public RestaurantRepositoryMock()
        {
            _restaurants = new Dictionary<int, IRestaurant>();
            var spbCity = new City(1, "Санкт-Петербург");
            var moscowCity = new City(2, "Москва");
            _restaurants.Add(1, new Restaurant(1, "Токи сити", spbCity));
            _restaurants.Add(2, new Restaurant(2, "Евразия", spbCity));
            _restaurants.Add(3, new Restaurant(3, "Тандур", spbCity));
            _restaurants.Add(4, new Restaurant(4, "Ткемали", moscowCity));
            _restaurants.Add(5, new Restaurant(5, "Марио", moscowCity));
        }

        /// <inheritdoc />
        public IRestaurant Add(IRestaurant restaurant)
        {
            var nextId = _restaurants.Keys.Max() + 1;
            var newRestaurant = new Restaurant(nextId, restaurant.Name, restaurant.City);
            _restaurants.Add(nextId, newRestaurant);
            return newRestaurant;
        }

        public Pagination<IRestaurant> GetAll(int skip, int limit)
        {
            var restaurants = _restaurants.Values.ToList()
                .Skip(skip).Take(limit).ToList();
            var totalCount = _restaurants.Values.ToList().Count;
            return new Pagination<IRestaurant>(restaurants, totalCount);
        }

        /// <inheritdoc />
        public IRestaurant Get(int id)
        {
            return _restaurants.TryGetValue(id, out var restaurant) ? restaurant : null;
        }

        public Pagination<IRestaurant> FindByCity(ICity city, int skip, int limit)
        {
            var restaurants = _restaurants.Where(r => r.Value.City.Id == city.Id)
                .Select(r => r.Value).ToList();
            var totalCount = restaurants.Count;
            return new Pagination<IRestaurant>(restaurants.Skip(skip).Take(limit).ToList(), totalCount);
        }

        /// <inheritdoc />
        public bool Exists(IRestaurant restaurant)
        {
            return _restaurants
                .Any(r=>r.Value.Name.Equals(restaurant.Name) && r.Value.City.Equals(restaurant.City));
        }
    }
}
