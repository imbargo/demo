﻿using Mts.Test.RestaurantCore.Models;
using Mts.Test.RestaurantCore.Repositories;

namespace Mts.Test.RestaurantCore
{
    /// <summary>
    /// Сервис по работе с городами
    /// </summary>
    public interface ICityService
    {
        /// <summary>
        /// Получает город по id
        /// </summary>
        /// <param name="id">Идентификатор города</param>
        /// <returns>Возвращяет город</returns>
        ICity Get(int id);

        /// <summary>
        /// Получает страницу c городами
        /// </summary>
        /// <param name="skip">Число элементов, которое нужно пропустить</param>
        /// <param name="limit">Число элементов, которое нужно взять</param>
        /// <returns>Возвращяет страницу с ресторанами</returns>
        Pagination<ICity> GetAll(int skip, int limit);

        /// <summary>
        /// Добавляет новый город
        /// </summary>
        /// <param name="city">Город для добавления</param>
        /// <returns>Возвращяет добавленный город</returns>
        ICity Add(ICity city);
    }
}
